class HomeController < ApplicationController
  def index
  end

  def purchase
    payment_method_token = add_credit_card(params[:purchase][:number])
    response = purchase_request(params[:purchase][:amount], payment_method_token, params[:browser_info])
    parsed_response = JSON.parse(response.body, symbolize_names: true)
    transaction = parsed_response[:transaction]

    case transaction[:state]
    when 'succeeded'
      render json: {message: 'success!'}
    when 'error'
      render json: {message: 'erro =('}
    when 'pending'
      redirect_to await_challenge_path({transaction: transaction})
    else
      render json: {message: "unknown state #{transaction[:state]}"}
    end
  end

  def await_challenge
    # @transaction_token = params[:transaction].present? ? params[:transaction][:token] : params[:transaction_token]
    @transaction_token = params[:transaction][:token]
  end

  def handle_callback
    render json: params
  end

  def handle_redirect
    render json: params
  end

  private

  def add_credit_card(number)
    response = conn.post('/v1/payment_methods.json') do |req|
      req.body = {
        payment_method: {
          credit_card: {
            first_name: "Joe",
            last_name: "Jones",
            number: number,
            verification_value: "123",
            month: "10",
            year: "2029",
            company: "Acme Inc.",
            address1: "33 Lane Road",
            address2: "Apartment 4",
            city: "Wanaque",
            state: "NJ",
            zip: "31331",
            country: "US",
            phone_number: "919.331.3313",
            shipping_address1: "33 Lane Road",
            shipping_address2: "Apartment 4",
            shipping_city: "Wanaque",
            shipping_state: "NJ",
            shipping_zip: "31331",
            shipping_country: "US",
            shipping_phone_number: "919.331.3313"
          },
          email: "joey@example.com",
          metadata: {
            heyyy: "oie =D",
            user_id: "4"
          }
        }
      }.to_json
    end

    JSON.parse(response.body)["transaction"]["payment_method"]["token"]
  end


  def purchase_request(amount, payment_method_token, browser_info)
    conn.post("/v1/gateways/#{ENV.fetch("GATEWAY")}/purchase.json") do |req|
      req_body = {
        transaction: {
          payment_method_token: payment_method_token,
          amount: amount,
          currency_code: "USD",
          attempt_3dsecure: true,
          callback_url: 'https://spreedly-3ds-poc.requestcatcher.com/handle_callback',
          redirect_url: 'https://spreedly-3ds-poc.requestcatcher.com/handle_redirect',
          # callback_url: handle_callback_url(host: 'spreedly-3ds-poc.requestcatcher.com', protocol: :http, port: 80),
          # redirect_url: await_challenge_url(host: 'spreedly-3ds-poc.requestcatcher.com', protocol: :http, port: 80),
          three_ds_version: "2",
          browser_info: browser_info
        }
      }
      req.body = req_body.to_json
    end
  end

  def conn
    @conn ||= Faraday.new(
      url: "https://core.spreedly.com",
      headers: {'Content-Type' => 'application/json'}
    ) do |conn|
      conn.basic_auth(ENV.fetch("ENV_KEY"), ENV.fetch("ACCESS_SECRET"))
    end
  end
end
